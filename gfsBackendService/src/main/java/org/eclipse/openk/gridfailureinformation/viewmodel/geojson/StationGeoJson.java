package org.eclipse.openk.gridfailureinformation.viewmodel.geojson;

import lombok.Data;

import java.util.List;

@Data
public class StationGeoJson {

    private String type;
    private String name;
    private List<Feature> features;

}