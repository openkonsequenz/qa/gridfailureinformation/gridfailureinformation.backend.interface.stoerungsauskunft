#  *******************************************************************************
#  Copyright (c) 2019 Contributors to the Eclipse Foundation
#
#  See the NOTICE file(s) distributed with this work for additional
#  information regarding copyright ownership.
#
#  This program and the accompanying materials are made available under the
#  terms of the Eclipse Public License v. 2.0 which is available at
#  http://www.eclipse.org/legal/epl-2.0.
#
#  SPDX-License-Identifier: EPL-2.0
#  *******************************************************************************
backend-version: 1.0.0
spring:
  config:
    activate:
      on-profile: localdev
  datasource:
    #url: jdbc:postgresql://entopticadirx:5432/GridFailureInfoDevelop
    url: jdbc:postgresql://entopticadirx:5432/GridFailureInfoDevServer
    username: ${GFI_DB_USERNAME}
    password: ${GFI_DB_PASSWORD}
    max-active: 5
  flyway:
    enabled: false
  main:
    allow-bean-definition-overriding: true

  # RabbitMQ configuration
  rabbitmq:
    host: entdockergss
    port: 5672
    username: ${GFI_RABBITMQ_USERNAME}
    password: ${GFI_RABBITMQ_PASSWORD}

    # Importchannel
    importExchange: sitImportExchange_localdev
    importQueue: sitImportQueue_localdev
    importkey: sitImportExchange.failureImportKey_localdev

    # Exportchannels
    exportExchange: sitExportExchange_localdev
    mail_export_queue: sit_mail_export_queue_localdev
    mail_export_routingkey: sit_mail_export_key_localdev
    channels:
      - name: Mail (lang)
        exportQueue: sit_mail_export_queue_localdev
        exportKey: sit_mail_export_key_localdev
        type: longMail # EMail
        initialDefault: true
      - name: Mail (kurz)
        exportQueue: sit_mail_export_queue_localdev
        exportKey: sit_mail_export_key_localdev
        type: shortMail  # For example SMS
        initialDefault: true
      - name: Störungsauskunft.de
        exportQueue: sit_stoerungsauskunft_export_queue_localdev
        exportKey: sit_stoerungsauskunft_export_key_localdev
        type: stoerungsauskunftde
        initialDefault: false
      - name: Störinfotool-eigene-Web-Komponenten #App und Internet
        exportQueue: sit_own_export_queue_localdev
        exportKey: sit_own_export_key_localdev
        type: webcomponent
        initialDefault: false

  # UI setting (Map)
  settings:
    overviewMapInitialZoom: 10
    detailMapInitialZoom: 10
    overviewMapInitialLatitude: 49.656634
    overviewMapInitialLongitude: 8.423207
    daysInPastToShowClosedInfos: 365
    #dataExternInitialVisibility: hide
    dataExternInitialVisibility: show

    plzLookupEnabled: true
    nominatimURL: https://nominatim.openstreetmap.org

    isUseHtmlEmailBtnTemplate: true
    # Mail settings (Default templates)
    emailSubjectPublishInit: "Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status veröffentlicht geändert."
    emailContentPublishInit: "<p><b>Veröffentlicht [TEST]</b></p>
                              <p> Sehr geehrte Damen und Herren,</p>
                              <p>die im Betreff genannte Meldung ist über folgenden Link erreichbar:</p>
                              $Direkter_Link_zur_Störung$
                              <p>Mit freundlichen Grüßen</p>
                              <p>Ihr Admin-Meister-Team der PTA GmbH</p>"
    emailSubjectUpdateInit: "Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status aktualisiert geändert."
    emailContentUpdateInit: "<p><b>Aktualisiert [TEST]</b></p>
                             <p> Sehr geehrte Damen und Herren,</p>
                             <p>die im Betreff genannte Meldung ist über folgenden Link erreichbar:</p>
                             $Direkter_Link_zur_Störung$
                             <p>Mit freundlichen Grüßen</p>
                             <p>Ihr Admin-Meister-Team der PTA GmbH</p>"
    emailSubjectCompleteInit: "Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status beendet geändert."
    emailContentCompleteInit: "<p><b>Beendet [TEST]</b></p>
                              <p> Sehr geehrte Damen und Herren,</p>
                              <p>die im Betreff genannte Meldung ist über folgenden Link erreichbar:</p>
                              $Direkter_Link_zur_Störung$
                              <p>Mit freundlichen Grüßen</p>
                              <p>Ihr Admin-Meister-Team der PTA GmbH</p>"

    # Mail settings short text (SMS) (Default templates)
    emailSubjectPublishShortInit: "[SHORT-TEST] Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status veröffentlicht geändert."
    emailContentPublishShortInit: "Veröffentlicht [SHORT-TEST]
                              Sehr geehrte Damen und Herren,
                              die im Betreff genannte Meldung ist über folgenden Link erreichbar:
                              $Direkter_Link_zur_Störung$
                              Mit freundlichen Grüßen
                              Ihr Admin-Meister-Team der PTA GmbH"
    emailSubjectUpdateShortInit: "[SHORT-TEST] Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status aktualisiert geändert."
    emailContentUpdateShortInit: "Aktualisiert [SHORT-TEST]
                                  Sehr geehrte Damen und Herren,
                                  die im Betreff genannte Meldung ist über folgenden Link erreichbar:
                                  $Direkter_Link_zur_Störung$
                                  Mit freundlichen Grüßen
                                  Ihr Admin-Meister-Team der PTA GmbH"
    emailSubjectCompleteShortInit: "[SHORT-TEST] Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status beendet geändert."
    emailContentCompleteShortInit: "Beendet [SHORT-TEST]
                                  Sehr geehrte Damen und Herren,
                                  die im Betreff genannte Meldung ist über folgenden Link erreichbar:
                                  $Direkter_Link_zur_Störung$
                                  Mit freundlichen Grüßen
                                  Ihr Admin-Meister-Team der PTA GmbH"

    visibilityConfiguration:
      fieldVisibility:
        failureClassification: show
        responsibility: show
        description: show
        internalRemark: show
      tableInternColumnVisibility:
        failureClassification: show
        responsibility: show
        description: show
        statusIntern: show
        statusExtern: show
        publicationStatus: show
        branch: show
        voltageLevel: show
        pressureLevel: show
        failureBegin: show
        failureEndPlanned: show
        failureEndResupplied: show
        expectedReasonText: show
        internalRemark: show
        postcode: show
        city: show
        district: show
        street: show
        housenumber: show
        radius: show
        stationIds: show
      tableExternColumnVisibility:
        branch: show
        failureBegin: show
        failureEndPlanned: show
        expectedReasonText: show
        description: show
        postcode: show
        city: show
        district: show
        street: show
        failureClassification: show
      mapExternTooltipVisibility:
        failureBegin: show
        failureEndPlanned: show
        expectedReasonText: show
        branch: show
        postcode: show
        city: show
        district: show
  jpa:
    show-sql: false

logging:
  level:
    root: INFO
    org.eclipse.openk: DEBUG
    org.springframework.web: ERROR
    org.hibernate: ERROR

server:
  port: 9165
  max-http-header-size: 262144
  servlet:
    session:
      tracking-modes: cookie

jwt:
  tokenHeader: Authorization
  useStaticJwt: true
  staticJwt: eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIYlI3Z2pobmE2eXJRZnZJTWhUSV9tY2g3ZmtTQWVFX3hLTjBhZVl0bjdjIn0.eyJqdGkiOiJlMmQ2YjE3Zi1iMWUyLTQxMzUtOTM1YS0zOWRiMmFkMzE5MjYiLCJleHAiOjE1ODYwMTI1MzUsIm5iZiI6MCwiaWF0IjoxNTg2MDEyMjM1LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvRWxvZ2Jvb2siLCJhdWQiOiJlbG9nYm9vay1iYWNrZW5kIiwic3ViIjoiMzMzMmRmOWItYmYyMy00Nzg5LThmMmQtM2JhZTM2MzA4YzNkIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZWxvZ2Jvb2stYmFja2VuZCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjYxMzRmN2RhLTdlNjQtNDJjNy05NjYyLTY0ZGNhZTk1MjQ5YSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiZ3JpZC1mYWlsdXJlLXF1YWxpZmllciIsImdyaWQtZmFpbHVyZS1jcmVhdG9yIiwiZ3JpZC1mYWlsdXJlLWFjY2VzcyIsImdyaWQtZmFpbHVyZS1hZG1pbiIsImdyaWQtZmFpbHVyZS1wdWJsaXNoZXIiXX0sInJlc291cmNlX2FjY2VzcyI6e30sIm5hbWUiOiJTdGF0aWMgSldUIiwicHJlZmVycmVkX3VzZXJuYW1lIjoidGVzdHVzZXJfZ2ZpX3N0YXRpY2p3dCIsImdpdmVuX25hbWUiOiJTdGF0aWMiLCJmYW1pbHlfbmFtZSI6IkpXVCJ9.SWpekZHxlDNWbaaZ2fHlts00c1Xi6yIb0ZMr9f8ujSI_6LVYuHx8FTt6g9tinyVcx5gQKfrooRW28Cdq1EVuexNtTtZ7ciKk4iEo_kgqUgRvHCwO7HQl2igpoGErheYD0kj3-u4Te6NPHKtXWIEfGyl0ZQBD2c4vtaCTQAy5Ilb146G7p_IcLYaZgpJPlGG0Bf2oZ0UqTQsrXxRJkXWARDz8D4lIrN84lAbqNlPlSZDN-8xp_z6mXgWpvgeZuFoYHItJMg2cjR0SXH-ycbWVXctRNQJfTWR0acIhp_nB_Xe6JlUx2vls99EVw-WUrd0hc8Y9658HdeyktvpQLDxP2g
  requiredRole:

gridFailureInformation:
  maxListSize: 2000

email:
  sender: sender@test.tester
  smtpHost: entdockergss
  port: 1025
  isHtmlEmail: true
  isUseHtmlEmailTemplate: true

# Services configuration
services:
  authNAuth:
    name: authNAuthService
    technical-username: admin_technical_portal
    technical-userpassword: admin_technical_portal
  contacts:
    name: contactService
    communicationType:
      mobile: Mobil
    useModuleNameForFilter: false
    moduleName: Störungsinformationstool
  sitCache:
    name: sitCacheService

feLoginURL: http://localhost:4200
portalFeLoginURL: http://entopkon:8880/portalFE/#/login
portalFeModulename: SIT DEV
authNAuthService:
  ribbon:
    listOfServers: http://entopkon:8880

contactService:
  ribbon:
    listOfServers: http://entdockergss:9156

sitCacheService:
  ribbon:
    #listOfServers: http://entdockergss:3003
    listOfServers: http://entdockergss:3013

cors:
  corsEnabled: true

process:
  definitions:
    classification:
      plannedMeasureDbid: 2

# Bei Mittelspannung oder bei den nachfolgend definierten Branches füge Adress-Polygonpunkte für Kartenanzeige hinzu
# Kommaseparierte Liste (Bsp.: "TV,TE,TI,TK,IN,ST,OS")
polygonBranches: "TK"

reminder:
  status-change:
    enabled: false
    minutes-before: 1440

distribution-group-publisher:
  name: Veröffentlicher
  distribution-text: Bitte anpassen

export-to-dmz:
  enabled: false
  password: cacheUpdateStrongPassword
  source: enetz
  cron: 0 */1 * ? * *

smsApiKey:
smsUrl:

swagger:
  enabled: true

failureAutoClose: false
failureAutoCloseHoursAfter: 48
failureAutoCloseCron: "*/15 * * * * *"

geoJsonImport:
  filepattern: '^geoj_(\d+)_(.*).(json)$'
  enabled: false
  #cron: "0 */1 * ? * *" #every minute
  cron: "*/10 * * ? * *" #10 sec
  folder: "C:\\Workspace\\Projekte\\OpenK\\EGitlab\\SIT\\test"
  #folder: "/usr/src/cbd/importFiles/"

---

spring:
  profiles: dev-db
  datasource:
    url: jdbc:postgresql://entopticadirx:5432/GridFailureInfoDevelop
    #url: jdbc:postgresql://entopticadirx:5432/GridFailureInfoDevServer
    username: ${GFI_DB_USERNAME}
    password: ${GFI_DB_PASSWORD}
  rabbitmq:
    host: entdockergss
    port: 5672
    username: ${GFI_RABBITMQ_USERNAME}
    password: ${GFI_RABBITMQ_PASSWORD}

    # Importkanal
    importExchange: sitImportExchange_localdev
    importQueue: sitImportQueue_localdev
    importkey: sitImportExchange.failureImportKey_localdev

    # Exportkanäle
    exportExchange: sitExportExchange_localdev
    channels:
      - name: Mail
        exportQueue: sit_mail_export_queue_localdev
        exportKey: sit_mail_export_key_localdev
        isMailType: true
      - name: Störungsauskunft.de
        exportQueue: sit_stoerungsauskunft_export_queue_localdev
        exportKey: sit_stoerungsauskunft_export_key_localdev
