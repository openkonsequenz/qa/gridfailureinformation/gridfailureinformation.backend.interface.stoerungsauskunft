/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.test.RabbitListenerTest;
import org.springframework.amqp.rabbit.test.RabbitListenerTestHarness;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = StoerungsauskunftInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
@RabbitListenerTest
public class MessageConsumerServiceTest {
    @Autowired
    @SpyBean
    private ImportExportService importExportService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestRabbitTemplate testRabbitTemplate;

    @Autowired
    private Response testResponse;

    @SpyBean
    private StoerungsauskunftApi stoerungsauskunftApi;

    @Value("${spring.rabbitmq.exportQueue}")
    private String exportQueue;

    @Test
    public void shouldExportForeignFailureMessageDto() throws JsonProcessingException {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();
        String rabbitMqMessageDtoString = objectMapper.writeValueAsString(rabbitMqMessageDto);

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        testRabbitTemplate.convertAndSend(exportQueue, rabbitMqMessageDtoString);

        verify(importExportService, times(1)).exportStoerungsauskunftOutage(any());
    }

    @Test
    public void shouldIgnoreButLogInputError() {
        testRabbitTemplate.convertAndSend(exportQueue, "false input");

        verify(importExportService, times(0)).exportStoerungsauskunftOutage(any());
    }
}
