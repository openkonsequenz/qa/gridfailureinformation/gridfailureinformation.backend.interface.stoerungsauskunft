package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.rabbitMq;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@Profile("!test")
@Configuration
@Log4j2
@EnableRabbit
@EnableIntegration
@Data
public class RabbitMqConfig {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitMqProperties rabbitMqProperties;

    @Bean
    public MessageChannel failureImportChannel() {
        return new DirectChannel();
    }

    @PostConstruct
    public void buildAllQueues() {
        log.info("RabbitMqConfig: Configuring all Exchanges and Queues");

        RabbitAdmin admin = new RabbitAdmin(rabbitTemplate);

        // Neuerstellung Exchange und Queues (Import) falls diese nicht vorhanden sind (passiert nur dann!)
        DirectExchange importExchange = new DirectExchange(rabbitMqProperties.getImportExchange());
        log.info("************** Configure Import RabbitMQ **************");
        log.info("ImportExchange: " + rabbitMqProperties.getImportExchange());
        admin.declareExchange(importExchange);
        Queue importQueue = new Queue(rabbitMqProperties.getImportQueue());
        log.info("ImportQueue: " + rabbitMqProperties.getImportQueue());
        log.info("ImportKey:   " + rabbitMqProperties.getImportKey());
        admin.declareQueue(importQueue);
        admin.declareBinding(BindingBuilder.bind(importQueue).to(importExchange).with(rabbitMqProperties.getImportKey()));

        // Neuerstellung Exchange und Queues (Export) falls diese nicht vorhanden sind (passiert nur dann!)
        DirectExchange exportExchange = new DirectExchange(rabbitMqProperties.getExportExchange());
        log.info("************** Configure Export RabbitMQ **************");
        log.info("ExportExchange: " + rabbitMqProperties.getExportExchange());
        admin.declareExchange(exportExchange);
        Queue exportQueue = new Queue(rabbitMqProperties.getExportQueue());
        admin.declareQueue(exportQueue);
        admin.declareBinding(BindingBuilder.bind(exportQueue).to(exportExchange).with(rabbitMqProperties.getExportkey()));

    }

    @Bean
    @ServiceActivator(inputChannel = "failureImportChannel")
    public MessageHandler messageHandler() {

        return message -> rabbitTemplate.convertAndSend(rabbitMqProperties.getImportExchange(),
                rabbitMqProperties.getImportKey(), message.getPayload(), message1 -> {
            message1.getMessageProperties().getHeaders().put("metaId", message.getHeaders().get("metaId"));
            message1.getMessageProperties().getHeaders().put("description", message.getHeaders().get("description"));
            message1.getMessageProperties().getHeaders().put("source", message.getHeaders().get("source"));
            return message1;
        });
    }
}
