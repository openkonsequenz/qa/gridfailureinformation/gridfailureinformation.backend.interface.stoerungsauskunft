/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ResourceLoaderBaseTest {
    @Test
    public void testLoadStringFromResourceNotFound() {
        ResourceLoaderBase rlb = new ResourceLoaderBase();
        String retString = rlb.loadStringFromResource("testNotExisting.txt");

        assertNull(retString);
    }

    @Test
    public void testLoadStringFromResource() {
        ResourceLoaderBase rlb = new ResourceLoaderBase();
        String retString = rlb.loadStringFromResource("testCase.txt");

        assertNotNull(retString);
    }
}
