package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class MessageConsumerService {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ImportExportService importExportService;

    @RabbitListener(queues = "${spring.rabbitmq.exportQueue}")
    public void listenMessage(Message message) {
        try {
            getMessage(message);
        } catch (Exception e) {
            log.error("Error while receiving a message from rabbitmq", e);
        }
    }

    public void getMessage(Message message) throws JsonProcessingException {

        SimpleMessageConverter converter = new SimpleMessageConverter();
        String messagePayload = (String) converter.fromMessage(message);
        RabbitMqMessageDto rabbitMqMessageDto = objectMapper.readValue(messagePayload, RabbitMqMessageDto.class);
        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }
}
