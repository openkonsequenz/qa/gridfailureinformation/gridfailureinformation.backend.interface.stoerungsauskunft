/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.service.ExpectedReasonService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExpectedReasonDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ExpectedReasonControllerTest {
    @Autowired
    @SpyBean
    private ExpectedReasonService expectedReasonService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new ExpectedReasonController(expectedReasonService))
                .build();
    }

    @Test
    public void shouldReturnExpectedReasons() throws Exception {
        Optional<String> branchOptional = Optional.empty();
        List<ExpectedReasonDto> expectedReasonDtoList = MockDataHelper.mockExpectedReasonDtoList();

        doReturn(expectedReasonDtoList).when(expectedReasonService).getExpectedReasons(branchOptional);

        mockMvc.perform(get("/expected-reasons"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}
