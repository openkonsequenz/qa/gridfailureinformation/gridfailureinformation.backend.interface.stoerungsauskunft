/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/hist-grid-failure-informations")
public class HistFailureInformationController {
    private final HistFailureInformationService histfailureInformationService;

    private final HistFailureInformationStationService histFailureInformationStationService;

    public HistFailureInformationController(HistFailureInformationService histfailureInformationService, HistFailureInformationStationService histFailureInformationStationService) {
        this.histfailureInformationService = histfailureInformationService;
        this.histFailureInformationStationService = histFailureInformationStationService;
    }


    @GetMapping("/{uuid}/versions")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen aller Versionen einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Störungsinformationen wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public List<FailureInformationDto> getHistFailureInformationVersions(@PathVariable String uuid) {

        if ("null".equals(uuid)) {
            return new ArrayList<>();
        }

        return histfailureInformationService.getFailureInformationVersionsByUuid(UUID.fromString(uuid));
    }

    @GetMapping("/{uuid}/versions/{versionNumber}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen einer bestimmten Version einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Störungsinformation wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public FailureInformationDto readVersion(
            @PathVariable UUID uuid,
            @PathVariable Long versionNumber) {
        return histfailureInformationService.getFailureInformationVersion(uuid, versionNumber);
    }


    @GetMapping("/{uuid}/versions/{versionNumber}/stations")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen der Stationen einer bestimmten Version einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Störungsinformation wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public List<StationDto> readStationsForVersion(
            @PathVariable UUID uuid,
            @PathVariable Long versionNumber) {
        return histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(uuid, versionNumber);
    }
}
