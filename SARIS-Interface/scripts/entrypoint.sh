#!/bin/sh

CERT_DIR="/opt/import_certs"
FORCE_REPLACE_CERT=${FORCE_REPLACE_CERT:-false}

# Überprüfe, ob das Zertifikat abgelaufen ist
function check_certificate {
    local alias="$1"
    local cert_file="$2"

    if keytool -list -alias "$alias" -keystore "$JAVA_HOME/lib/security/cacerts" -storepass changeit > /dev/null 2>&1; then

        if [ "$FORCE_REPLACE_CERT" = true ]; then
            echo "Zertifikat '$alias': FORCE_REPLACE_CERT ist true. Lösche und importiere..."
            keytool -delete -alias "$alias" -keystore "$JAVA_HOME/lib/security/cacerts" -storepass changeit -noprompt
            keytool -importcert -file "$cert_file" -keystore "$JAVA_HOME/lib/security/cacerts" -storepass changeit -alias "$alias" -noprompt
        else
            echo "Zertifikat vorhanden. Keine Aktion erforderlich."
        fi
    else
        echo "Zertifikat '$alias' nicht gefunden. Importiere..."
        keytool -importcert -file "$cert_file" -keystore "$JAVA_HOME/lib/security/cacerts" -storepass changeit -alias "$alias" -noprompt
    fi
}

# Importiere Zertifikate aus dem angegebenen Verzeichnis und überprüfe auf FORCE_REPLACE_CERT
CERT_DIR="/opt/import_certs"

if [ -d "$CERT_DIR" ]; then
    for CERT_FILE in "$CERT_DIR"/*.crt; do
        if [ -f "$CERT_FILE" ]; then
            ALIAS=$(basename "$CERT_FILE" .crt)
            check_certificate "$ALIAS" "$CERT_FILE"
        fi
    done
fi
echo "Start application..."
java -jar "grid-failure-information.backend.saris-interface.jar"
