package org.eclipse.openk.gridfailureinformation.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.SpringVersion;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.TimeZone;
import java.util.stream.StreamSupport;

@Slf4j
@Component
public class InitEventHandler {

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        ApplicationContext context = event.getApplicationContext();
        log.info("ApplicationContext: " + context.getDisplayName());
        if (!(context instanceof AnnotationConfigServletWebServerApplicationContext)) {
            return;
        }
        printProperties(event);
    }

    private void printProperties(ContextRefreshedEvent event) {
        final Environment environment = event.getApplicationContext().getEnvironment();
        log.info("Timezone.getDefault(): " + TimeZone.getDefault());
        log.info("Spring Version: "+ SpringVersion.getVersion());
        log.info("============= Log Level Check (info, warn, error, debug, trace) =============");
        log.info("info");
        log.warn("warn");
        log.error("error");
        log.debug("debug");
        log.trace("trace");
        log.info("============= Log Level Check End =========\n");
        log.info("============== Configuration ==============");
        log.info("Active profile: {}", Arrays.toString(environment.getActiveProfiles()));
        final MutablePropertySources sources = ((AbstractEnvironment) environment).getPropertySources();
        StreamSupport.stream(sources.spliterator(), false)
                .filter(MapPropertySource.class::isInstance)
                .filter(propertySource -> propertySource.getName().contains("application"))
                .map(ps -> ((MapPropertySource) ps).getPropertyNames())
                .flatMap(Arrays::stream)
                .distinct()
                .filter(prop -> !prop.contains("password") && !prop.contains("secret") && !prop.contains("credentials"))
                .forEach(prop -> log.info("{}: {}", prop, environment.getProperty(prop)));
        log.info("===========================================\n");
    }
}
