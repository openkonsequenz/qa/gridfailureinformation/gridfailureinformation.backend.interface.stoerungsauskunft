/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.jobs;

import org.aspectj.lang.annotation.Before;
import org.eclipse.openk.gridfailureinformation.importadresses.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressImportService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import javax.xml.ws.http.HTTPException;

import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.FALSE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class JobManagerTest {
    @Autowired
    @SpyBean
    private JobManager jobManager;

    @Autowired
    @SpyBean
    private AddressImportService addressImportService;

    @Test
    public void shouldTriggerStartImport() {
        jobManager.triggerStartImport();
        Boolean startImport = Whitebox.getInternalState(jobManager, "startImport");
        assertEquals(TRUE, startImport);
    }

    @Test
    public void shouldImportOnTriggerAndSetStartImportBackToFalse() {
        ReflectionTestUtils.setField(jobManager, "startImport", TRUE);

        ReflectionTestUtils.invokeMethod(jobManager, "importOnTrigger");

        var startImport = ReflectionTestUtils.getField(jobManager, "startImport");
        assertNotNull(startImport);
        assertFalse((Boolean) startImport);
    }

    @Test
    public void shouldImportOnTriggerAndDontChangeTheValueFromStartImport() {
        Whitebox.setInternalState(jobManager, "startImport", FALSE);

        try {
            Whitebox.invokeMethod(jobManager, "importOnTrigger");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Boolean startImport = Whitebox.getInternalState(jobManager, "startImport");
        assertEquals(FALSE, startImport);
    }

    @Test
    public void shouldImportAddressdataAndInvokeImportAddresses() {
        jobManager.cleanUp = false;

        ReflectionTestUtils.invokeMethod(jobManager, "importAddressdata");

        verify(addressImportService, times(1)).importAddresses(false);
    }


    @Test
    public void shouldNotImportAddressdataDueToAnException() {
        jobManager.cleanUp = false;

        doThrow(new HTTPException(HttpStatus.PROCESSING.value())).when(addressImportService).importAddresses(anyBoolean());

        ReflectionTestUtils.invokeMethod(jobManager, "importAddressdata");

        verify(addressImportService, times(1)).importAddresses(false);
    }


    @Test
    public void shouldNotImportAddressdataDueToAnUnknownException() {
        jobManager.cleanUp = false;

        doThrow(new HTTPException(HttpStatus.INTERNAL_SERVER_ERROR.value())).when(addressImportService).importAddresses(anyBoolean());

        assertThrows(HTTPException.class, () -> ReflectionTestUtils.invokeMethod(jobManager, "importAddressdata"));
    }
}
