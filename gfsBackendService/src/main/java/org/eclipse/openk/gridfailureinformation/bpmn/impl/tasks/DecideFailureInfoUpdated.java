/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;

@Log4j2
public class DecideFailureInfoUpdated extends DecisionTask<GfiProcessSubject> {

    public DecideFailureInfoUpdated() {
        super("Decision: Ist die Störungsinformation aktualisiert?");
    }

    @Override
    public OutputPort decide(GfiProcessSubject subject) throws ProcessException {
        ProcessState newState = subject.getProcessHelper().getProcessStateFromStatusUuid(
                subject.getFailureInformationDto().getStatusInternId());

        String loggerOutput1 = "Decide: ";

        if (newState == GfiProcessState.UPDATED) {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing PORT1");
            return OutputPort.PORT1;
        }
        else if (newState == GfiProcessState.COMPLETED) {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing PORT2");
            return OutputPort.PORT2;
        }
        else if (newState == GfiProcessState.CANCELED) {
            log.debug(( loggerOutput1 + getDescription() + "\" -> Firing PORT3"));
            return OutputPort.PORT3;
        }
        else {
            throw new ProcessException(this.getDescription() + ": Invalid status request:" + newState,
                    new InternalServerErrorException("invalid.status.request"));
        }
    }

}
