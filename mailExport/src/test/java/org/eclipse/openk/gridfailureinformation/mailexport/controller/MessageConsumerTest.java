/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.eclipse.openk.gridfailureinformation.mailexport.MailExportApplication;
import org.eclipse.openk.gridfailureinformation.mailexport.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.mailexport.service.EmailService;
import org.eclipse.openk.gridfailureinformation.mailexport.support.MockDataHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest(classes = MailExportApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class MessageConsumerTest {
    @Autowired
    private MessageConsumer messageConsumer;

    @Autowired
    @SpyBean
    private EmailService emailService;

    private final ObjectMapper objectMapperForTest = new ObjectMapper();

    private static GreenMail mailServer;

    @BeforeAll
    public static void beforeAll() {
        ServerSetup serverSetup = new ServerSetup(3025 , "localhost", ServerSetup.PROTOCOL_SMTP);
        serverSetup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(serverSetup);
    }

    @BeforeEach
    public void beforeTest() {
        mailServer.start();
    }

    @AfterEach
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    public void shouldCallImport() throws Exception {
        String mockMailMessageDtoString = objectMapperForTest.writeValueAsString(MockDataHelper.mockMailMessageDto());
        MessageProperties properties = new MessageProperties();
        properties.setContentType("text");

        Message mockMessage = mock(Message.class);
        when(mockMessage.getBody()).thenReturn(mockMailMessageDtoString.getBytes());
        when(mockMessage.getMessageProperties()).thenReturn(properties);

        messageConsumer.listenMessage(mockMessage);

        verify(emailService, times(1)).sendMail(any());
    }
}
