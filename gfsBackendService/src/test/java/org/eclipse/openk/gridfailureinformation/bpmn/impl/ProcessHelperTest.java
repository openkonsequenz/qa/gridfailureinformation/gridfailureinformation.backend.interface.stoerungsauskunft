/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class ProcessHelperTest {
    @Autowired
    @SpyBean
    private ProcessHelper processHelper;

    @Autowired
    private FailureClassificationRepository failureClassificationRepository;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private FailureInformationPublicationChannelRepository channelRepository;

    @Autowired
    @SpyBean
    private FailureInformationService failureInformationService;

    @Test
    void shouldDetectIfFailureInfoIsPlanned_True() {
        RefFailureClassification fc = MockDataHelper.mockRefFailureClassification();
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        dto.setFailureClassificationId(fc.getUuid());
        when(failureClassificationRepository.findById(anyLong()))
                .thenReturn(Optional.of(fc));

        assertTrue(processHelper.getEnvironment().getFailureInformationService().isFailureInfoPlanned(dto));
    }

    @Test
    void shouldDetectIfFailureInfoIsPlanned_False() {
        RefFailureClassification fc = MockDataHelper.mockRefFailureClassification();
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        dto.setFailureClassificationId(UUID.randomUUID());
        when( failureClassificationRepository.findById(anyLong()))
                .thenReturn(Optional.of(fc));

        assertFalse(processHelper.getEnvironment().getFailureInformationService().isFailureInfoPlanned(dto));
    }

    @Test
    void shouldDetectIfFailureInfoIsPlanned_Exception() {
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        when( failureClassificationRepository.findById(anyLong()))
                .thenReturn(Optional.empty());

        assertThrows( InternalServerErrorException.class, () -> processHelper.getEnvironment().getFailureInformationService().isFailureInfoPlanned(dto));
    }

    @Test
    void shouldGetProcessStateFromStatus_Exception() {
        when( statusRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.empty());
        final UUID statusUuid = UUID.randomUUID();
        assertThrows(InternalServerErrorException.class, () -> processHelper.getProcessStateFromStatusUuid(statusUuid));
    }

    @Test
    void shouldResetProcessStateWithException() {
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        when( failureInformationRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class,
                () -> processHelper.resetPublishedStateForChannels(dto));
    }

    @Test
    void shouldResetProcessStateCorrectly() {
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation tblFi = MockDataHelper.mockTblFailureInformation();
        when( failureInformationRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.of(tblFi));

        List<TblFailureInformationPublicationChannel> distChannels = MockDataHelper.mockTblFailureInformationPublicationChannelList();
        when( channelRepository.findByTblFailureInformation(eq(tblFi) )).thenReturn(distChannels);

        processHelper.resetPublishedStateForChannels(dto);

        verify(channelRepository, times( distChannels.size())).save(any( TblFailureInformationPublicationChannel.class));
    }

    @Test
    void shouldGetSubordinatedChildrenCorrectly() {
        FailureInformationDto parentDto = MockDataHelper.mockFailureInformationDto();
        List<FailureInformationDto> dtos = MockDataHelper.mockGridFailureInformationDtos();

        doReturn(dtos).when(failureInformationService).findFailureInformationsByCondensedUuid(eq(parentDto.getUuid()));

        List<FailureInformationDto> dtoResults = processHelper.getSubordinatedChildren(parentDto);

        assertEquals(dtos.get(0).getUuid(), dtoResults.get(0).getUuid());
    }
}
