/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("/stoerungsauskunft")
public class ImportExportController {
    private final ImportExportService importExportService;

    public ImportExportController(ImportExportService importExportService) {
        this.importExportService = importExportService;
    }

    @GetMapping("/usernotification-import-test")
    @Operation(summary = "Import einer externen Störungsinformation von Störungsauskunft.de zu SIT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void importUserNotification() {
        importExportService.importUserNotifications();
    }

    @PostMapping("/outage-export-test")
    @Operation(summary = "Export einer Störungsinformation von SIT zu Störungsauskunft.de")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich exportiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void postOutage(@RequestBody RabbitMqMessageDto rabbitMqMessageDto) throws InternalServerErrorException {
        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }
}
